// test2.click

in_device ::   FromDevice(eth1);
        in_device
	-> Strip(14)
	-> Align(4, 0)    // in case we're not on x86
        -> Print(ok)
        -> ToDevice(eth2);
